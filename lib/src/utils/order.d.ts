import { BigNumber, BigNumberish } from "ethers";
import type { ConsiderationItem, CreateInputItem, Fee, Item, OfferItem, Order, OrderParameters } from "../types";
import { OrderStatus } from "../types";
export declare const feeToConsiderationItem: ({ fee, token, baseAmount, baseEndAmount, }: {
    fee: Fee;
    token: string;
    baseAmount: BigNumberish;
    baseEndAmount?: BigNumberish | undefined;
}) => ConsiderationItem;
export declare const deductFees: <T extends Item>(items: T[], fees?: readonly Fee[] | undefined) => T[];
export declare const mapInputItemToOfferItem: (item: CreateInputItem) => OfferItem;
export declare const areAllCurrenciesSame: ({ offer, consideration, }: Pick<OrderParameters, "offer" | "consideration">) => boolean;
export declare const totalItemsAmount: <T extends OfferItem>(items: T[]) => {
    startAmount: BigNumber;
    endAmount: BigNumber;
};
export declare const mapOrderAmountsFromFilledStatus: (order: Order, { totalFilled, totalSize }: {
    totalFilled: BigNumber;
    totalSize: BigNumber;
}) => Order;
export declare const mapOrderAmountsFromUnitsToFill: (order: Order, { unitsToFill, totalFilled, totalSize, }: {
    unitsToFill: BigNumberish;
    totalFilled: BigNumber;
    totalSize: BigNumber;
}) => Order;
export declare const generateRandomSalt: () => string;
export declare function validateAndSanitizeFromOrderStatus(order: Order, orderStatus: OrderStatus): Order;
export declare const openseaAssetToAsset: (asset: any) => {
    tokenId: any;
    tokenAddress: any;
    schemaName: any;
    collection: {
        royaltyFeePoints: any;
        royaltyFeeAddress: any;
    };
};
