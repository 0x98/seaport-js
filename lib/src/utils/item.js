"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getMaximumSizeForOrder = exports.getSummedTokenAndIdentifierAmounts = exports.getPresentItemAmount = exports.isCriteriaItem = exports.isErc1155Item = exports.isErc721Item = exports.isErc20Item = exports.isNativeCurrencyItem = exports.isCurrencyItem = void 0;
const ethers_1 = require("ethers");
const constants_1 = require("../constants");
const criteria_1 = require("./criteria");
const gcd_1 = require("./gcd");
const isCurrencyItem = ({ itemType }) => [constants_1.ItemType.NATIVE, constants_1.ItemType.ERC20].includes(itemType);
exports.isCurrencyItem = isCurrencyItem;
const isNativeCurrencyItem = ({ itemType }) => itemType === constants_1.ItemType.NATIVE;
exports.isNativeCurrencyItem = isNativeCurrencyItem;
const isErc20Item = (itemType) => itemType === constants_1.ItemType.ERC20;
exports.isErc20Item = isErc20Item;
const isErc721Item = (itemType) => [constants_1.ItemType.ERC721, constants_1.ItemType.ERC721_WITH_CRITERIA].includes(itemType);
exports.isErc721Item = isErc721Item;
const isErc1155Item = (itemType) => [constants_1.ItemType.ERC1155, constants_1.ItemType.ERC1155_WITH_CRITERIA].includes(itemType);
exports.isErc1155Item = isErc1155Item;
const isCriteriaItem = (itemType) => [constants_1.ItemType.ERC721_WITH_CRITERIA, constants_1.ItemType.ERC1155_WITH_CRITERIA].includes(itemType);
exports.isCriteriaItem = isCriteriaItem;
const getPresentItemAmount = ({ startAmount, endAmount, timeBasedItemParams, }) => {
    const startAmountBn = ethers_1.BigNumber.from(startAmount);
    const endAmountBn = ethers_1.BigNumber.from(endAmount);
    if (!timeBasedItemParams) {
        return startAmountBn.gt(endAmountBn) ? startAmountBn : endAmountBn;
    }
    const { isConsiderationItem, currentBlockTimestamp, ascendingAmountTimestampBuffer, startTime, endTime, } = timeBasedItemParams;
    const duration = ethers_1.BigNumber.from(endTime).sub(startTime);
    const isAscending = endAmountBn.gt(startAmount);
    const adjustedBlockTimestamp = ethers_1.BigNumber.from(isAscending
        ? currentBlockTimestamp + ascendingAmountTimestampBuffer
        : currentBlockTimestamp);
    if (adjustedBlockTimestamp.lt(startTime)) {
        return startAmountBn;
    }
    const elapsed = (adjustedBlockTimestamp.gt(endTime)
        ? ethers_1.BigNumber.from(endTime)
        : adjustedBlockTimestamp).sub(startTime);
    const remaining = duration.sub(elapsed);
    return startAmountBn
        .mul(remaining)
        .add(endAmountBn.mul(elapsed))
        .add(isConsiderationItem ? duration.sub(1) : 0)
        .div(duration);
};
exports.getPresentItemAmount = getPresentItemAmount;
const getSummedTokenAndIdentifierAmounts = ({ items, criterias, timeBasedItemParams, }) => {
    const itemToCriteria = (0, criteria_1.getItemToCriteriaMap)(items, criterias);
    const tokenAndIdentifierToSummedAmount = items.reduce((map, item) => {
        var _a, _b, _c, _d;
        const identifierOrCriteria = (_b = (_a = itemToCriteria.get(item)) === null || _a === void 0 ? void 0 : _a.identifier) !== null && _b !== void 0 ? _b : item.identifierOrCriteria;
        return Object.assign(Object.assign({}, map), { [item.token]: Object.assign(Object.assign({}, map[item.token]), { [identifierOrCriteria]: ((_d = (_c = map[item.token]) === null || _c === void 0 ? void 0 : _c[identifierOrCriteria]) !== null && _d !== void 0 ? _d : ethers_1.BigNumber.from(0)).add((0, exports.getPresentItemAmount)({
                    startAmount: item.startAmount,
                    endAmount: item.endAmount,
                    timeBasedItemParams,
                })) }) });
    }, {});
    return tokenAndIdentifierToSummedAmount;
};
exports.getSummedTokenAndIdentifierAmounts = getSummedTokenAndIdentifierAmounts;
const getMaximumSizeForOrder = ({ parameters: { offer, consideration } }) => {
    const allItems = [...offer, ...consideration];
    const amounts = allItems.flatMap(({ startAmount, endAmount }) => [
        startAmount,
        endAmount,
    ]);
    return (0, gcd_1.findGcd)(amounts);
};
exports.getMaximumSizeForOrder = getMaximumSizeForOrder;
