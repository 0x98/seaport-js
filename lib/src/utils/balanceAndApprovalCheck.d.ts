import { BigNumber } from "ethers";
import { ItemType } from "../constants";
import type { InputCriteria, OrderParameters } from "../types";
import { getSummedTokenAndIdentifierAmounts, TimeBasedItemParams } from "./item";
export declare type BalancesAndApprovals = {
    token: string;
    identifierOrCriteria: string;
    balance: BigNumber;
    approvedAmount: BigNumber;
    itemType: ItemType;
}[];
export declare type InsufficientBalances = {
    token: string;
    identifierOrCriteria: string;
    requiredAmount: BigNumber;
    amountHave: BigNumber;
    itemType: ItemType;
}[];
export declare type InsufficientApprovals = {
    token: string;
    identifierOrCriteria: string;
    approvedAmount: BigNumber;
    requiredApprovedAmount: BigNumber;
    operator: string;
    itemType: ItemType;
}[];
export declare const getInsufficientBalanceAndApprovalAmounts: ({ balancesAndApprovals, tokenAndIdentifierAmounts, operator, }: {
    balancesAndApprovals: BalancesAndApprovals;
    tokenAndIdentifierAmounts: ReturnType<typeof getSummedTokenAndIdentifierAmounts>;
    operator: string;
}) => {
    insufficientBalances: InsufficientBalances;
    insufficientApprovals: InsufficientApprovals;
};
export declare const validateOfferBalancesAndApprovals: ({ offer, criterias, balancesAndApprovals, timeBasedItemParams, throwOnInsufficientBalances, throwOnInsufficientApprovals, operator, }: {
    balancesAndApprovals: BalancesAndApprovals;
    timeBasedItemParams?: TimeBasedItemParams | undefined;
    throwOnInsufficientBalances?: boolean | undefined;
    throwOnInsufficientApprovals?: boolean | undefined;
    operator: string;
} & Pick<OrderParameters, "offer"> & {
    criterias: InputCriteria[];
}) => InsufficientApprovals;
export declare const validateBasicFulfillBalancesAndApprovals: ({ offer, consideration, offererBalancesAndApprovals, fulfillerBalancesAndApprovals, timeBasedItemParams, offererOperator, fulfillerOperator, }: {
    offererBalancesAndApprovals: BalancesAndApprovals;
    fulfillerBalancesAndApprovals: BalancesAndApprovals;
    timeBasedItemParams: TimeBasedItemParams;
    offererOperator: string;
    fulfillerOperator: string;
} & Pick<OrderParameters, "offer" | "consideration">) => InsufficientApprovals;
export declare const validateStandardFulfillBalancesAndApprovals: ({ offer, consideration, offerCriteria, considerationCriteria, offererBalancesAndApprovals, fulfillerBalancesAndApprovals, timeBasedItemParams, offererOperator, fulfillerOperator, }: Pick<OrderParameters, "offer" | "consideration"> & {
    offerCriteria: InputCriteria[];
    considerationCriteria: InputCriteria[];
    offererBalancesAndApprovals: BalancesAndApprovals;
    fulfillerBalancesAndApprovals: BalancesAndApprovals;
    timeBasedItemParams: TimeBasedItemParams;
    offererOperator: string;
    fulfillerOperator: string;
}) => InsufficientApprovals;
