"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAdvancedOrderNumeratorDenominator = exports.generateFulfillOrdersFulfillments = exports.shouldUseBasicFulfill = void 0;
const ethers_1 = require("ethers");
const constants_1 = require("../constants");
const criteria_1 = require("./criteria");
const gcd_1 = require("./gcd");
const item_1 = require("./item");
const order_1 = require("./order");
const shouldUseBasicFulfill = ({ offer, consideration, offerer }, totalFilled) => {
    if (!totalFilled.eq(0)) {
        return false;
    }
    if (offer.length > 1 || consideration.length === 0) {
        return false;
    }
    const allItems = [...offer, ...consideration];
    const nfts = allItems.filter(({ itemType }) => [constants_1.ItemType.ERC721, constants_1.ItemType.ERC1155].includes(itemType));
    const nftsWithCriteria = allItems.filter(({ itemType }) => (0, item_1.isCriteriaItem)(itemType));
    const offersNativeCurrency = (0, item_1.isNativeCurrencyItem)(offer[0]);
    if (offersNativeCurrency) {
        return false;
    }
    if (nfts.length !== 1 || nftsWithCriteria.length !== 0) {
        return false;
    }
    if (!(0, order_1.areAllCurrenciesSame)({ offer, consideration })) {
        return false;
    }
    const differentStartAndEndAmount = allItems.some(({ startAmount, endAmount }) => startAmount !== endAmount);
    if (differentStartAndEndAmount) {
        return false;
    }
    const [firstConsideration, ...restConsideration] = consideration;
    const firstConsiderationRecipientIsNotOfferer = firstConsideration.recipient.toLowerCase() !== offerer.toLowerCase();
    if (firstConsiderationRecipientIsNotOfferer) {
        return false;
    }
    if (consideration.length > 1 &&
        restConsideration.every((item) => item.itemType === offer[0].itemType) &&
        (0, order_1.totalItemsAmount)(restConsideration).endAmount.gt(offer[0].endAmount)) {
        return false;
    }
    const currencies = allItems.filter(item_1.isCurrencyItem);
    const nativeCurrencyIsZeroAddress = currencies
        .filter(({ itemType }) => itemType === constants_1.ItemType.NATIVE)
        .every(({ token }) => token === ethers_1.ethers.constants.AddressZero);
    const currencyIdentifiersAreZero = currencies.every(({ identifierOrCriteria }) => ethers_1.BigNumber.from(identifierOrCriteria).eq(0));
    const erc721sAreSingleAmount = nfts
        .filter(({ itemType }) => itemType === constants_1.ItemType.ERC721)
        .every(({ endAmount }) => endAmount === "1");
    return (nativeCurrencyIsZeroAddress &&
        currencyIdentifiersAreZero &&
        erc721sAreSingleAmount);
};
exports.shouldUseBasicFulfill = shouldUseBasicFulfill;
function generateFulfillOrdersFulfillments(ordersMetadata) {
    const hashAggregateKey = ({ sourceOrDestination, operator = "", token, identifier, }) => `${sourceOrDestination}-${operator}-${token}-${identifier}`;
    const offerAggregatedFulfillments = {};
    const considerationAggregatedFulfillments = {};
    ordersMetadata.forEach(({ order, offererOperator, offerCriteria }, orderIndex) => {
        const itemToCriteria = (0, criteria_1.getItemToCriteriaMap)(order.parameters.offer, offerCriteria);
        return order.parameters.offer.forEach((item, itemIndex) => {
            var _a, _b, _c;
            const aggregateKey = `${hashAggregateKey({
                sourceOrDestination: order.parameters.offerer,
                operator: offererOperator,
                token: item.token,
                identifier: (_b = (_a = itemToCriteria.get(item)) === null || _a === void 0 ? void 0 : _a.identifier) !== null && _b !== void 0 ? _b : item.identifierOrCriteria,
            })}${(0, item_1.isErc721Item)(item.itemType) ? itemIndex : ""}`;
            offerAggregatedFulfillments[aggregateKey] = [
                ...((_c = offerAggregatedFulfillments[aggregateKey]) !== null && _c !== void 0 ? _c : []),
                { orderIndex, itemIndex },
            ];
        });
    });
    ordersMetadata.forEach(({ order, considerationCriteria, tips }, orderIndex) => {
        const itemToCriteria = (0, criteria_1.getItemToCriteriaMap)(order.parameters.consideration, considerationCriteria);
        return [...order.parameters.consideration, ...tips].forEach((item, itemIndex) => {
            var _a, _b, _c;
            const aggregateKey = `${hashAggregateKey({
                sourceOrDestination: item.recipient,
                token: item.token,
                identifier: (_b = (_a = itemToCriteria.get(item)) === null || _a === void 0 ? void 0 : _a.identifier) !== null && _b !== void 0 ? _b : item.identifierOrCriteria,
            })}${(0, item_1.isErc721Item)(item.itemType) ? itemIndex : ""}`;
            considerationAggregatedFulfillments[aggregateKey] = [
                ...((_c = considerationAggregatedFulfillments[aggregateKey]) !== null && _c !== void 0 ? _c : []),
                { orderIndex, itemIndex },
            ];
        });
    });
    return {
        offerFulfillments: Object.values(offerAggregatedFulfillments),
        considerationFulfillments: Object.values(considerationAggregatedFulfillments),
    };
}
exports.generateFulfillOrdersFulfillments = generateFulfillOrdersFulfillments;
const getAdvancedOrderNumeratorDenominator = (order, unitsToFill) => {
    const maxUnits = (0, item_1.getMaximumSizeForOrder)(order);
    const unitsToFillBn = ethers_1.BigNumber.from(unitsToFill);
    const unitsGcd = (0, gcd_1.gcd)(unitsToFillBn, maxUnits);
    const numerator = unitsToFill
        ? unitsToFillBn.div(unitsGcd)
        : ethers_1.BigNumber.from(1);
    const denominator = unitsToFill ? maxUnits.div(unitsGcd) : ethers_1.BigNumber.from(1);
    return { numerator, denominator };
};
exports.getAdvancedOrderNumeratorDenominator = getAdvancedOrderNumeratorDenominator;
