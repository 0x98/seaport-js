import MerkleTreeJS from "merkletreejs";
export declare class MerkleTree {
    tree: MerkleTreeJS;
    constructor(identifiers: string[]);
    getProof(identifier: string): string[];
    getRoot(): string;
}
