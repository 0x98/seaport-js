"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateStandardFulfillBalancesAndApprovals = exports.validateBasicFulfillBalancesAndApprovals = exports.validateOfferBalancesAndApprovals = exports.getInsufficientBalanceAndApprovalAmounts = void 0;
const item_1 = require("./item");
const findBalanceAndApproval = (balancesAndApprovals, token, identifierOrCriteria) => {
    const balanceAndApproval = balancesAndApprovals.find(({ token: checkedToken, identifierOrCriteria: checkedIdentifierOrCriteria, }) => token.toLowerCase() === checkedToken.toLowerCase() &&
        checkedIdentifierOrCriteria.toLowerCase() ===
            identifierOrCriteria.toLowerCase());
    if (!balanceAndApproval) {
        throw new Error("Balances and approvals didn't contain all tokens and identifiers");
    }
    return balanceAndApproval;
};
const getInsufficientBalanceAndApprovalAmounts = ({ balancesAndApprovals, tokenAndIdentifierAmounts, operator, }) => {
    const tokenAndIdentifierAndAmountNeeded = [
        ...Object.entries(tokenAndIdentifierAmounts).map(([token, identifierToAmount]) => Object.entries(identifierToAmount).map(([identifierOrCriteria, amountNeeded]) => [token, identifierOrCriteria, amountNeeded])),
    ].flat();
    const filterBalancesOrApprovals = (filterKey) => tokenAndIdentifierAndAmountNeeded
        .filter(([token, identifierOrCriteria, amountNeeded]) => findBalanceAndApproval(balancesAndApprovals, token, identifierOrCriteria)[filterKey].lt(amountNeeded))
        .map(([token, identifierOrCriteria, amount]) => {
        const balanceAndApproval = findBalanceAndApproval(balancesAndApprovals, token, identifierOrCriteria);
        return {
            token,
            identifierOrCriteria,
            requiredAmount: amount,
            amountHave: balanceAndApproval[filterKey],
            itemType: balanceAndApproval.itemType,
        };
    });
    const mapToApproval = (insufficientBalance) => ({
        token: insufficientBalance.token,
        identifierOrCriteria: insufficientBalance.identifierOrCriteria,
        approvedAmount: insufficientBalance.amountHave,
        requiredApprovedAmount: insufficientBalance.requiredAmount,
        itemType: insufficientBalance.itemType,
        operator,
    });
    const [insufficientBalances, insufficientApprovals] = [
        filterBalancesOrApprovals("balance"),
        filterBalancesOrApprovals("approvedAmount").map(mapToApproval),
    ];
    return {
        insufficientBalances,
        insufficientApprovals,
    };
};
exports.getInsufficientBalanceAndApprovalAmounts = getInsufficientBalanceAndApprovalAmounts;
const validateOfferBalancesAndApprovals = ({ offer, criterias, balancesAndApprovals, timeBasedItemParams, throwOnInsufficientBalances = true, throwOnInsufficientApprovals, operator, }) => {
    const { insufficientBalances, insufficientApprovals } = (0, exports.getInsufficientBalanceAndApprovalAmounts)({
        balancesAndApprovals,
        tokenAndIdentifierAmounts: (0, item_1.getSummedTokenAndIdentifierAmounts)({
            items: offer,
            criterias,
            timeBasedItemParams: timeBasedItemParams
                ? Object.assign(Object.assign({}, timeBasedItemParams), { isConsiderationItem: false }) : undefined,
        }),
        operator,
    });
    if (throwOnInsufficientBalances && insufficientBalances.length > 0) {
        throw new Error("The offerer does not have the amount needed to create or fulfill.");
    }
    if (throwOnInsufficientApprovals && insufficientApprovals.length > 0) {
        throw new Error("The offerer does not have the sufficient approvals.");
    }
    return insufficientApprovals;
};
exports.validateOfferBalancesAndApprovals = validateOfferBalancesAndApprovals;
const validateBasicFulfillBalancesAndApprovals = ({ offer, consideration, offererBalancesAndApprovals, fulfillerBalancesAndApprovals, timeBasedItemParams, offererOperator, fulfillerOperator, }) => {
    (0, exports.validateOfferBalancesAndApprovals)({
        offer,
        criterias: [],
        balancesAndApprovals: offererBalancesAndApprovals,
        timeBasedItemParams,
        throwOnInsufficientApprovals: true,
        operator: offererOperator,
    });
    const considerationWithoutOfferItemType = consideration.filter((item) => item.itemType !== offer[0].itemType);
    const { insufficientBalances, insufficientApprovals } = (0, exports.getInsufficientBalanceAndApprovalAmounts)({
        balancesAndApprovals: fulfillerBalancesAndApprovals,
        tokenAndIdentifierAmounts: (0, item_1.getSummedTokenAndIdentifierAmounts)({
            items: considerationWithoutOfferItemType,
            criterias: [],
            timeBasedItemParams: Object.assign(Object.assign({}, timeBasedItemParams), { isConsiderationItem: true }),
        }),
        operator: fulfillerOperator,
    });
    if (insufficientBalances.length > 0) {
        throw new Error("The fulfiller does not have the balances needed to fulfill.");
    }
    return insufficientApprovals;
};
exports.validateBasicFulfillBalancesAndApprovals = validateBasicFulfillBalancesAndApprovals;
const validateStandardFulfillBalancesAndApprovals = ({ offer, consideration, offerCriteria, considerationCriteria, offererBalancesAndApprovals, fulfillerBalancesAndApprovals, timeBasedItemParams, offererOperator, fulfillerOperator, }) => {
    (0, exports.validateOfferBalancesAndApprovals)({
        offer,
        criterias: offerCriteria,
        balancesAndApprovals: offererBalancesAndApprovals,
        timeBasedItemParams,
        throwOnInsufficientApprovals: true,
        operator: offererOperator,
    });
    const fulfillerBalancesAndApprovalsAfterReceivingOfferedItems = addToExistingBalances({
        items: offer,
        criterias: offerCriteria,
        balancesAndApprovals: fulfillerBalancesAndApprovals,
        timeBasedItemParams,
    });
    const { insufficientBalances, insufficientApprovals } = (0, exports.getInsufficientBalanceAndApprovalAmounts)({
        balancesAndApprovals: fulfillerBalancesAndApprovalsAfterReceivingOfferedItems,
        tokenAndIdentifierAmounts: (0, item_1.getSummedTokenAndIdentifierAmounts)({
            items: consideration,
            criterias: considerationCriteria,
            timeBasedItemParams: Object.assign(Object.assign({}, timeBasedItemParams), { isConsiderationItem: true }),
        }),
        operator: fulfillerOperator,
    });
    if (insufficientBalances.length > 0) {
        throw new Error("The fulfiller does not have the balances needed to fulfill.");
    }
    return insufficientApprovals;
};
exports.validateStandardFulfillBalancesAndApprovals = validateStandardFulfillBalancesAndApprovals;
const addToExistingBalances = ({ items, criterias, timeBasedItemParams, balancesAndApprovals, }) => {
    const summedItemAmounts = (0, item_1.getSummedTokenAndIdentifierAmounts)({
        items,
        criterias,
        timeBasedItemParams: Object.assign(Object.assign({}, timeBasedItemParams), { isConsiderationItem: false }),
    });
    const balancesAndApprovalsAfterReceivingItems = balancesAndApprovals.map((item) => (Object.assign({}, item)));
    Object.entries(summedItemAmounts).forEach(([token, identifierOrCriteriaToAmount]) => Object.entries(identifierOrCriteriaToAmount).forEach(([identifierOrCriteria, amount]) => {
        const balanceAndApproval = findBalanceAndApproval(balancesAndApprovalsAfterReceivingItems, token, identifierOrCriteria);
        const balanceAndApprovalIndex = balancesAndApprovalsAfterReceivingItems.indexOf(balanceAndApproval);
        balancesAndApprovalsAfterReceivingItems[balanceAndApprovalIndex].balance =
            balancesAndApprovalsAfterReceivingItems[balanceAndApprovalIndex].balance.add(amount);
    }));
    return balancesAndApprovalsAfterReceivingItems;
};
