"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.findGcd = exports.gcd = void 0;
const ethers_1 = require("ethers");
const gcd = (a, b) => {
    const bnA = ethers_1.BigNumber.from(a);
    const bnB = ethers_1.BigNumber.from(b);
    if (bnA.eq(0)) {
        return bnB;
    }
    return (0, exports.gcd)(bnB.mod(a), bnA);
};
exports.gcd = gcd;
const findGcd = (elements) => {
    let result = ethers_1.BigNumber.from(elements[0]);
    for (let i = 1; i < elements.length; i++) {
        result = (0, exports.gcd)(elements[i], result);
        if (result.eq(1)) {
            return result;
        }
    }
    return result;
};
exports.findGcd = findGcd;
