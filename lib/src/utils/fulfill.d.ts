import { BigNumber, BigNumberish } from "ethers";
import type { Order, OrderParameters, OrderStatus, FulfillOrdersMetadata } from "../types";
export declare const shouldUseBasicFulfill: ({ offer, consideration, offerer }: OrderParameters, totalFilled: OrderStatus["totalFilled"]) => boolean;
export declare function generateFulfillOrdersFulfillments(ordersMetadata: FulfillOrdersMetadata): {
    offerFulfillments: any[];
    considerationFulfillments: any[];
};
export declare const getAdvancedOrderNumeratorDenominator: (order: Order, unitsToFill?: BigNumberish | undefined) => {
    numerator: BigNumber;
    denominator: BigNumber;
};
