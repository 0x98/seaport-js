"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.openseaAssetToAsset = exports.validateAndSanitizeFromOrderStatus = exports.generateRandomSalt = exports.mapOrderAmountsFromUnitsToFill = exports.mapOrderAmountsFromFilledStatus = exports.totalItemsAmount = exports.areAllCurrenciesSame = exports.mapInputItemToOfferItem = exports.deductFees = exports.feeToConsiderationItem = void 0;
const ethers_1 = require("ethers");
const constants_1 = require("../constants");
const item_1 = require("./item");
const merkletree_1 = require("./merkletree");
const multiplyBasisPoints = (amount, basisPoints) => ethers_1.BigNumber.from(amount)
    .mul(ethers_1.BigNumber.from(basisPoints))
    .div(constants_1.ONE_HUNDRED_PERCENT_BP);
const feeToConsiderationItem = ({ fee, token, baseAmount, baseEndAmount = baseAmount, }) => {
    return {
        itemType: token === ethers_1.ethers.constants.AddressZero ? constants_1.ItemType.NATIVE : constants_1.ItemType.ERC20,
        token,
        identifierOrCriteria: "0",
        startAmount: multiplyBasisPoints(baseAmount, fee.basisPoints).toString(),
        endAmount: multiplyBasisPoints(baseEndAmount, fee.basisPoints).toString(),
        recipient: fee.recipient,
    };
};
exports.feeToConsiderationItem = feeToConsiderationItem;
const deductFees = (items, fees) => {
    if (!fees) {
        return items;
    }
    const totalBasisPoints = fees.reduce((accBasisPoints, fee) => accBasisPoints + fee.basisPoints, 0);
    return items.map((item) => (Object.assign(Object.assign({}, item), { startAmount: (0, item_1.isCurrencyItem)(item)
            ? ethers_1.BigNumber.from(item.startAmount)
                .sub(multiplyBasisPoints(item.startAmount, totalBasisPoints))
                .toString()
            : item.startAmount, endAmount: (0, item_1.isCurrencyItem)(item)
            ? ethers_1.BigNumber.from(item.endAmount)
                .sub(multiplyBasisPoints(item.endAmount, totalBasisPoints))
                .toString()
            : item.endAmount })));
};
exports.deductFees = deductFees;
const mapInputItemToOfferItem = (item) => {
    var _a, _b, _c, _d, _e, _f, _g;
    if ("itemType" in item) {
        if ("identifiers" in item) {
            const tree = new merkletree_1.MerkleTree(item.identifiers);
            return {
                itemType: item.itemType === constants_1.ItemType.ERC721
                    ? constants_1.ItemType.ERC721_WITH_CRITERIA
                    : constants_1.ItemType.ERC1155_WITH_CRITERIA,
                token: item.token,
                identifierOrCriteria: tree.getRoot(),
                startAmount: (_a = item.amount) !== null && _a !== void 0 ? _a : "1",
                endAmount: (_c = (_b = item.endAmount) !== null && _b !== void 0 ? _b : item.amount) !== null && _c !== void 0 ? _c : "1",
            };
        }
        if ("amount" in item || "endAmount" in item) {
            return {
                itemType: item.itemType,
                token: item.token,
                identifierOrCriteria: item.identifier,
                startAmount: item.amount,
                endAmount: (_e = (_d = item.endAmount) !== null && _d !== void 0 ? _d : item.amount) !== null && _e !== void 0 ? _e : "1",
            };
        }
        return {
            itemType: item.itemType,
            token: item.token,
            identifierOrCriteria: item.identifier,
            startAmount: "1",
            endAmount: "1",
        };
    }
    return {
        itemType: item.token && item.token !== ethers_1.ethers.constants.AddressZero
            ? constants_1.ItemType.ERC20
            : constants_1.ItemType.NATIVE,
        token: (_f = item.token) !== null && _f !== void 0 ? _f : ethers_1.ethers.constants.AddressZero,
        identifierOrCriteria: "0",
        startAmount: item.amount,
        endAmount: (_g = item.endAmount) !== null && _g !== void 0 ? _g : item.amount,
    };
};
exports.mapInputItemToOfferItem = mapInputItemToOfferItem;
const areAllCurrenciesSame = ({ offer, consideration, }) => {
    const allItems = [...offer, ...consideration];
    const currencies = allItems.filter(item_1.isCurrencyItem);
    return currencies.every(({ itemType, token }) => itemType === currencies[0].itemType &&
        token.toLowerCase() === currencies[0].token.toLowerCase());
};
exports.areAllCurrenciesSame = areAllCurrenciesSame;
const totalItemsAmount = (items) => {
    const initialValues = {
        startAmount: ethers_1.BigNumber.from(0),
        endAmount: ethers_1.BigNumber.from(0),
    };
    return items
        .map(({ startAmount, endAmount }) => ({
        startAmount,
        endAmount,
    }))
        .reduce(({ startAmount: totalStartAmount, endAmount: totalEndAmount }, { startAmount, endAmount }) => ({
        startAmount: totalStartAmount.add(startAmount),
        endAmount: totalEndAmount.add(endAmount),
    }), {
        startAmount: ethers_1.BigNumber.from(0),
        endAmount: ethers_1.BigNumber.from(0),
    });
};
exports.totalItemsAmount = totalItemsAmount;
const mapOrderAmountsFromFilledStatus = (order, { totalFilled, totalSize }) => {
    if (totalFilled.eq(0) || totalSize.eq(0)) {
        return order;
    }
    const basisPoints = totalSize
        .sub(totalFilled)
        .mul(constants_1.ONE_HUNDRED_PERCENT_BP)
        .div(totalSize);
    return {
        parameters: Object.assign(Object.assign({}, order.parameters), { offer: order.parameters.offer.map((item) => (Object.assign(Object.assign({}, item), { startAmount: multiplyBasisPoints(item.startAmount, basisPoints).toString(), endAmount: multiplyBasisPoints(item.endAmount, basisPoints).toString() }))), consideration: order.parameters.consideration.map((item) => (Object.assign(Object.assign({}, item), { startAmount: multiplyBasisPoints(item.startAmount, basisPoints).toString(), endAmount: multiplyBasisPoints(item.endAmount, basisPoints).toString() }))) }),
        signature: order.signature,
    };
};
exports.mapOrderAmountsFromFilledStatus = mapOrderAmountsFromFilledStatus;
const mapOrderAmountsFromUnitsToFill = (order, { unitsToFill, totalFilled, totalSize, }) => {
    const unitsToFillBn = ethers_1.BigNumber.from(unitsToFill);
    if (unitsToFillBn.lte(0)) {
        throw new Error("Units to fill must be greater than 1");
    }
    const maxUnits = (0, item_1.getMaximumSizeForOrder)(order);
    if (totalSize.eq(0)) {
        totalSize = maxUnits;
    }
    const remainingOrderPercentageToBeFilled = totalSize
        .sub(totalFilled)
        .mul(constants_1.ONE_HUNDRED_PERCENT_BP)
        .div(totalSize);
    const unitsToFillBasisPoints = unitsToFillBn
        .mul(constants_1.ONE_HUNDRED_PERCENT_BP)
        .div(maxUnits);
    const basisPoints = remainingOrderPercentageToBeFilled.gt(unitsToFillBasisPoints)
        ? unitsToFillBasisPoints
        : remainingOrderPercentageToBeFilled;
    return {
        parameters: Object.assign(Object.assign({}, order.parameters), { offer: order.parameters.offer.map((item) => (Object.assign(Object.assign({}, item), { startAmount: multiplyBasisPoints(item.startAmount, basisPoints).toString(), endAmount: multiplyBasisPoints(item.endAmount, basisPoints).toString() }))), consideration: order.parameters.consideration.map((item) => (Object.assign(Object.assign({}, item), { startAmount: multiplyBasisPoints(item.startAmount, basisPoints).toString(), endAmount: multiplyBasisPoints(item.endAmount, basisPoints).toString() }))) }),
        signature: order.signature,
    };
};
exports.mapOrderAmountsFromUnitsToFill = mapOrderAmountsFromUnitsToFill;
const generateRandomSalt = () => {
    return `0x${Buffer.from(ethers_1.ethers.utils.randomBytes(16)).toString("hex")}`;
};
exports.generateRandomSalt = generateRandomSalt;
function validateAndSanitizeFromOrderStatus(order, orderStatus) {
    const { isValidated, isCancelled, totalFilled, totalSize } = orderStatus;
    if (totalSize.gt(0) && totalFilled.div(totalSize).eq(1)) {
        throw new Error("The order you are trying to fulfill is already filled");
    }
    if (isCancelled) {
        throw new Error("The order you are trying to fulfill is cancelled");
    }
    if (isValidated) {
        return { parameters: Object.assign({}, order.parameters), signature: "0x" };
    }
    return order;
}
exports.validateAndSanitizeFromOrderStatus = validateAndSanitizeFromOrderStatus;
const openseaAssetToAsset = (asset) => {
    return {
        "tokenId": asset.token_id,
        "tokenAddress": asset.address,
        "schemaName": asset.schema_name,
        "collection": {
            "royaltyFeePoints": asset.royaltyFeePoints,
            "royaltyFeeAddress": asset.royaltyFeeAddress
        }
    };
};
exports.openseaAssetToAsset = openseaAssetToAsset;
