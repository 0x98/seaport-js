import { BigNumber } from "ethers";
import type { InputCriteria, Item, Order, OrderParameters } from "../types";
export declare const isCurrencyItem: ({ itemType }: Item) => boolean;
export declare const isNativeCurrencyItem: ({ itemType }: Item) => boolean;
export declare const isErc20Item: (itemType: Item["itemType"]) => boolean;
export declare const isErc721Item: (itemType: Item["itemType"]) => boolean;
export declare const isErc1155Item: (itemType: Item["itemType"]) => boolean;
export declare const isCriteriaItem: (itemType: Item["itemType"]) => boolean;
export declare type TimeBasedItemParams = {
    isConsiderationItem?: boolean;
    currentBlockTimestamp: number;
    ascendingAmountTimestampBuffer: number;
} & Pick<OrderParameters, "startTime" | "endTime">;
export declare const getPresentItemAmount: ({ startAmount, endAmount, timeBasedItemParams, }: Pick<Item, "startAmount" | "endAmount"> & {
    timeBasedItemParams?: TimeBasedItemParams | undefined;
}) => BigNumber;
export declare const getSummedTokenAndIdentifierAmounts: ({ items, criterias, timeBasedItemParams, }: {
    items: Item[];
    criterias: InputCriteria[];
    timeBasedItemParams?: TimeBasedItemParams | undefined;
}) => Record<string, Record<string, BigNumber>>;
export declare const getMaximumSizeForOrder: ({ parameters: { offer, consideration } }: Order) => BigNumber;
