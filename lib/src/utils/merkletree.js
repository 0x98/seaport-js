"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerkleTree = void 0;
const ethers_1 = require("ethers");
const utils_1 = require("ethers/lib/utils");
const merkletreejs_1 = __importDefault(require("merkletreejs"));
const hashIdentifier = (identifier) => (0, utils_1.keccak256)(Buffer.from(ethers_1.BigNumber.from(identifier).toHexString().slice(2).padStart(64, "0"), "hex"));
class MerkleTree {
    constructor(identifiers) {
        this.tree = new merkletreejs_1.default(identifiers.map(hashIdentifier), utils_1.keccak256, {
            sort: true,
        });
    }
    getProof(identifier) {
        return this.tree.getHexProof(hashIdentifier(identifier));
    }
    getRoot() {
        return this.tree.getRoot().toString("hex") ? this.tree.getHexRoot() : "0";
    }
}
exports.MerkleTree = MerkleTree;
