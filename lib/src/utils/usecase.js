"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getTransactionMethods = exports.executeAllActions = void 0;
const executeAllActions = async (actions) => {
    for (let i = 0; i < actions.length - 1; i++) {
        const action = actions[i];
        if (action.type === "approval") {
            await action.transactionMethods.transact();
        }
    }
    const finalAction = actions[actions.length - 1];
    return finalAction.type === "create"
        ? await finalAction.createOrder()
        : await finalAction.transactionMethods.transact();
};
exports.executeAllActions = executeAllActions;
const instanceOfOverrides = (obj) => {
    const validKeys = [
        "gasLimit",
        "gasPrice",
        "maxFeePerGas",
        "maxPriorityFeePerGas",
        "nonce",
        "type",
        "accessList",
        "customData",
        "ccipReadEnabled",
        "value",
        "blockTag",
        "CallOverrides",
    ];
    return (obj === undefined ||
        Object.keys(obj).every((key) => validKeys.includes(key)));
};
const getTransactionMethods = (contract, method, args) => {
    const lastArg = args[args.length - 1];
    let initialOverrides;
    if (instanceOfOverrides(lastArg)) {
        initialOverrides = lastArg;
        args.pop();
    }
    return {
        callStatic: (overrides) => {
            const mergedOverrides = Object.assign(Object.assign({}, initialOverrides), overrides);
            return contract.callStatic[method](...[...args, mergedOverrides]);
        },
        estimateGas: (overrides) => {
            const mergedOverrides = Object.assign(Object.assign({}, initialOverrides), overrides);
            return contract.estimateGas[method](...[...args, mergedOverrides]);
        },
        transact: (overrides) => {
            const mergedOverrides = Object.assign(Object.assign({}, initialOverrides), overrides);
            return contract[method](...args, mergedOverrides);
        },
        buildTransaction: (overrides) => {
            const mergedOverrides = Object.assign(Object.assign({}, initialOverrides), overrides);
            return contract.populateTransaction[method](...[...args, mergedOverrides]);
        },
    };
};
exports.getTransactionMethods = getTransactionMethods;
