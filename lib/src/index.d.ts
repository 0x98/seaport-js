/// <reference types="node" />
import EventEmitter from 'events';
import { Seaport } from "./seaport";
import { SeaportAPI } from "./api/seaport";
import { SwapEx } from "./swapEx/swapEx";
import { Asset, FeesInfo, APIConfig, Web3Accounts, ExchangetAgent, OrderSide, CreateOrderParams, MatchParams, SellOrderParams, BuyOrderParams, MatchOrderOption } from "web3-accounts";
import { AssetsQueryParams, AssetCollection } from "./api/types";
import { WalletInfo } from "web3-wallets";
import { OrderWithCounter, MatchOrdersParams } from "./types";
export declare class SeaportSDK extends EventEmitter implements ExchangetAgent {
    walletInfo: WalletInfo;
    contracts: Seaport;
    swap: SwapEx;
    api: SeaportAPI;
    user: Web3Accounts;
    constructor(wallet: WalletInfo, config?: APIConfig);
    getOrderApprove(params: CreateOrderParams, side: OrderSide): Promise<import("web3-accounts").ApproveInfo>;
    getMatchCallData(params: MatchParams): Promise<any>;
    createSellOrder(params: SellOrderParams): Promise<OrderWithCounter>;
    createBuyOrder(params: BuyOrderParams): Promise<OrderWithCounter>;
    fulfillOrder(orderStr: string, options?: MatchOrderOption): Promise<import("web3-wallets").TransactionResponse>;
    fulfillOrders(orders: MatchOrdersParams): Promise<import("web3-wallets").TransactionResponse>;
    cancelOrders(orders: string[]): Promise<any>;
    getAssetBalances(asset: Asset, account?: string): Promise<string>;
    getTokenBalances(params: {
        tokenAddress: string;
        accountAddress?: string;
        rpcUrl?: string;
    }): Promise<any>;
    transfer(asset: Asset, to: string, quantity: number): Promise<import("web3-wallets").TransactionResponse>;
    getOwnerAssets(tokens?: AssetsQueryParams): Promise<AssetCollection[]>;
    getAssetsFees(tokens: AssetsQueryParams): Promise<FeesInfo[]>;
}
