"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Seaport = exports.generateRandomSalt = exports.computeFees = void 0;
const events_1 = __importDefault(require("events"));
const index_1 = require("./contracts/index");
const web3_accounts_1 = require("web3-accounts");
const web3_wallets_1 = require("web3-wallets");
const ethers_1 = require("ethers");
const types_1 = require("./types");
const constants_1 = require("./constants");
const criteria_1 = require("./utils/criteria");
const item_1 = require("./utils/item");
const order_1 = require("./utils/order");
const fulfill_1 = require("./utils/fulfill");
function computeFees(recipients, tokenTotal, tokenAddress) {
    let erc20TokenAmount = tokenTotal;
    const fees = recipients.map(val => {
        const amount = tokenTotal.mul(val.points).div(constants_1.ONE_HUNDRED_PERCENT_BP);
        erc20TokenAmount = erc20TokenAmount.sub(amount);
        return {
            itemType: tokenAddress == web3_wallets_1.NULL_ADDRESS ? constants_1.ItemType.NATIVE : constants_1.ItemType.ERC20,
            token: tokenAddress,
            identifierOrCriteria: "0",
            startAmount: amount.toString(),
            endAmount: amount.toString(),
            recipient: val.address
        };
    });
    return { fees, erc20TokenAmount };
}
exports.computeFees = computeFees;
const generateRandomSalt = () => {
    return web3_wallets_1.ethers.BigNumber.from(web3_wallets_1.ethers.utils.randomBytes(7)).toString();
};
exports.generateRandomSalt = generateRandomSalt;
class Seaport extends events_1.default {
    constructor(wallet, config) {
        super();
        this.protocolFeePoints = 250;
        const contracts = (config === null || config === void 0 ? void 0 : config.contractAddresses) || index_1.SEAPORT_CONTRACTS_ADDRESSES[wallet.chainId];
        this.protocolFeeAddress = contracts.FeeRecipientAddress;
        if (config === null || config === void 0 ? void 0 : config.protocolFeePoints) {
            this.protocolFeePoints = config.protocolFeePoints;
            this.protocolFeeAddress = config.protocolFeeAddress || contracts.FeeRecipientAddress;
        }
        this.walletInfo = wallet;
        const chainId = wallet.chainId;
        if (!contracts) {
            throw chainId + 'Opensea sdk undefine contracts address';
        }
        const { Exchange, Conduit, ConduitController, Zone, PausableZone, GasToken } = contracts;
        this.zoneAddress = Zone;
        this.pausableZoneAddress = PausableZone;
        this.contractAddresses = contracts;
        this.GasWarpperToken = {
            name: 'GasToken',
            symbol: 'GasToken',
            address: GasToken,
            decimals: 18
        };
        this.userAccount = new web3_accounts_1.Web3Accounts(wallet);
        const options = this.userAccount.signer;
        if (ConduitController && Exchange && Conduit) {
            this.seaport = new web3_wallets_1.ethers.Contract(Exchange, index_1.SeaportABI.seaport.abi, options);
            this.conduit = new web3_wallets_1.ethers.Contract(Conduit, index_1.SeaportABI.conduit.abi, options);
            this.conduitController = new web3_wallets_1.ethers.Contract(ConduitController, index_1.SeaportABI.conduitController.abi, options);
        }
        else {
            throw new Error(`${this.walletInfo.chainId} abi undefined`);
        }
        this.config = {
            ascendingAmountFulfillmentBuffer: 60 * 5,
            balanceAndApprovalChecksOnOrderCreation: true,
            conduitKeyToConduit: Object.assign(Object.assign({}, constants_1.KNOWN_CONDUIT_KEYS_TO_CONDUIT), { [constants_1.NO_CONDUIT]: this.seaport.address }),
        };
        this.defaultConduitKey = constants_1.NO_CONDUIT;
    }
    async getOrderApprove({ asset, quantity = 1, paymentToken = this.GasWarpperToken, startAmount, }, side) {
        const operator = this.conduit.address;
        const decimals = paymentToken ? paymentToken.decimals : 18;
        if (side == web3_accounts_1.OrderSide.Sell) {
            const assetApprove = await this.userAccount.getAssetApprove(asset, operator);
            if (Number(assetApprove.balances) < Number(quantity)) {
                throw 'Seller asset is not enough';
            }
            return assetApprove;
        }
        else {
            const { allowance, calldata, balances } = await this.userAccount.getTokenApprove(paymentToken.address, operator);
            const amount = web3_wallets_1.ethers.utils.parseUnits(startAmount.toString(), paymentToken.decimals);
            if (amount.gt(balances)) {
                throw 'CheckOrderMatch: buyer erc20Token  gt balances';
            }
            const spend = web3_wallets_1.ethers.utils.parseUnits(startAmount.toString(), decimals);
            return {
                isApprove: spend.lt(allowance),
                balances,
                calldata: spend.lt(allowance) ? undefined : calldata
            };
        }
    }
    async createOrder(offer, consideration, expirationTime, listingTime) {
        const offerer = this.walletInfo.address;
        const operator = this.conduit.address;
        for (const offerAsset of offer) {
            let approve = false, data;
            if (offerAsset.itemType == constants_1.ItemType.ERC20) {
                const { balances, allowance, calldata } = await this.userAccount.getTokenApprove(offerAsset.token, operator);
                if (web3_wallets_1.ethers.BigNumber.from(offerAsset.endAmount).gt(balances)) {
                    throw new Error("Offer amount less than balances");
                }
                if (web3_wallets_1.ethers.BigNumber.from(offerAsset.endAmount).gt(allowance)) {
                    approve = false;
                    data = calldata;
                }
            }
            else {
                const { isApprove, balances, calldata } = await this.userAccount.getAssetApprove({
                    tokenAddress: offerAsset.token,
                    tokenId: offerAsset.identifierOrCriteria,
                    schemaName: offerAsset.itemType == constants_1.ItemType.ERC721 ? "ERC721" : "ERC115"
                }, operator);
                if (web3_wallets_1.ethers.BigNumber.from(offerAsset.endAmount).gt(balances)) {
                    throw new Error("Offer amount less than balances");
                }
                if (!isApprove) {
                    approve = false;
                    data = calldata;
                }
            }
            if (!approve && data) {
                const tx = await this.ethSend(data);
                await tx.wait();

            }
        }
        const orderType = types_1.OrderType.FULL_RESTRICTED;
        const startTime = (listingTime || Math.round(Date.now() / 1000)).toString();
        const endTime = (expirationTime || Math.round(Date.now() / 1000 + 60 * 60 * 24 * 7)).toString();
        const conduitKey = await this.conduitController.getKey(this.conduit.address);
        let zone = this.pausableZoneAddress;
        if (offer[0].itemType == constants_1.ItemType.ERC20) {
            zone = this.zoneAddress;
        }
        const orderParameters = {
            offerer,
            zone,
            orderType,
            startTime,
            endTime,
            zoneHash: web3_wallets_1.NULL_BLOCK_HASH,
            salt: (0, exports.generateRandomSalt)(),
            offer,
            consideration,
            conduitKey
        };
        const resolvedCounter = await this.getCounter(this.walletInfo.address);
        const signature = await this.signOrder(orderParameters, resolvedCounter);
        return {
            parameters: Object.assign(Object.assign({}, orderParameters), { counter: resolvedCounter }),
            signature,
        };
    }
    async createBuyOrder({ asset, quantity = 1, paymentToken = this.GasWarpperToken, expirationTime = 0, startAmount }) {
        const tokenAmount = web3_wallets_1.ethers.utils.parseUnits(startAmount.toString(), paymentToken.decimals);
        const offer = [
            {
                itemType: constants_1.ItemType.ERC20,
                token: paymentToken.address,
                identifierOrCriteria: "0",
                startAmount: tokenAmount.toString(),
                endAmount: tokenAmount.toString()
            }
        ];
        if (!(asset === null || asset === void 0 ? void 0 : asset.tokenId))
            throw new Error("The token ID cannot be empty");
        const consideration = [{
                itemType: asset.schemaName.toLowerCase() == "erc721" ? constants_1.ItemType.ERC721 : constants_1.ItemType.ERC1155,
                token: asset.tokenAddress,
                identifierOrCriteria: asset.tokenId,
                startAmount: quantity.toString(),
                endAmount: quantity.toString(),
                recipient: this.walletInfo.address
            }];
        const recipients = [{
                address: this.protocolFeeAddress,
                points: this.protocolFeePoints
            }];
        const collection = asset.collection;
        if (collection && collection.royaltyFeePoints && collection.royaltyFeeAddress) {
            const { royaltyFeeAddress, royaltyFeePoints } = collection;
            recipients.push({
                address: royaltyFeeAddress,
                points: royaltyFeePoints
            });
        }
        const { fees } = computeFees(recipients, tokenAmount, paymentToken.address);
        consideration.push(...fees);
        return this.createOrder(offer, consideration, expirationTime);
    }
    async createSellOrder({ asset, quantity = 1, paymentToken = web3_accounts_1.NullToken, expirationTime = 0, startAmount, listingTime }) {
        var _a;
        const assetAmount = quantity.toString();
        const offer = [{
                itemType: asset.schemaName.toLowerCase() == "erc721" ? constants_1.ItemType.ERC721 : constants_1.ItemType.ERC1155,
                token: asset.tokenAddress,
                identifierOrCriteria: ((_a = asset === null || asset === void 0 ? void 0 : asset.tokenId) === null || _a === void 0 ? void 0 : _a.toString()) || "1",
                startAmount: assetAmount,
                endAmount: assetAmount
            }];
        const recipients = [{
                address: this.protocolFeeAddress,
                points: this.protocolFeePoints
            }];
        const { collection } = asset;
        if (collection && collection.royaltyFeePoints && collection.royaltyFeeAddress) {
            const { royaltyFeeAddress, royaltyFeePoints } = collection;
            recipients.push({
                address: royaltyFeeAddress,
                points: royaltyFeePoints
            });
        }
        const payPoints = recipients.map(val => Number(val.points)).reduce((cur, next) => cur + next);
        recipients.unshift({
            address: this.walletInfo.address,
            points: constants_1.ONE_HUNDRED_PERCENT_BP - payPoints
        });
        const tokeneAmount = web3_wallets_1.ethers.utils.parseUnits(startAmount.toString(), paymentToken.decimals);
        const { fees: consideration } = computeFees(recipients, tokeneAmount, paymentToken.address);
        return this.createOrder(offer, consideration, expirationTime, listingTime);
    }
    async signOrder(orderParameters, counter) {
        const domainData = {
            name: constants_1.SEAPORT_CONTRACT_NAME,
            version: constants_1.SEAPORT_CONTRACT_VERSION,
            chainId: this.walletInfo.chainId,
            verifyingContract: this.seaport.address,
        };
        const orderComponents = Object.assign(Object.assign({}, orderParameters), { counter });
        const { signature } = await this.userAccount.signTypedData({
            types: constants_1.EIP_712_ORDER_TYPE,
            domain: domainData,
            primaryType: constants_1.EIP_712_PRIMARY_TYPE,
            message: orderComponents
        });
        return signature;
    }
    async checkOrderPost(order, taker = web3_wallets_1.NULL_ADDRESS) {
        const { parameters, signature } = JSON.parse(order);
        const operator = this.conduit.address;
        const { offer } = parameters;
        const offerAsset = offer[0];
        if (offerAsset.itemType == constants_1.ItemType.ERC20) {
            const { balances, allowance } = await this.userAccount.getTokenApprove(offerAsset.token, operator);

        }
        else {
            const { isApprove, balances, calldata } = await this.userAccount.getAssetApprove({
                tokenAddress: offerAsset.token,
                tokenId: offerAsset.identifierOrCriteria,
                schemaName: offerAsset.itemType == constants_1.ItemType.ERC721 ? "ERC721" : "ERC115"
            }, operator);

        }
    }
    async getMatchCallData({ order, takerAmount, tips = [], }) {
        if (takerAmount) {
            return this.fulfillAdvancedOrder({ order, takerAmount, tips });
        }
        else {
            return this.fulfillBasicOrder({ order, tips });
        }
    }
    async fulfillAvailableAdvancedOrders({ orders, takerAmount, recipient, tips = [], }) {
        var _a, _b;
        const ordersMetadata = [];
        const offers = [], considerations = [];
        let totalNativeAmount = ethers_1.BigNumber.from(0);
        for (const order of orders) {
            const { parameters } = order;
            const orderStatus = await this.getOrderStatus(this.getOrderHash(parameters));
            const { totalFilled, totalSize } = orderStatus;
            const orderWithAdjustedFills = takerAmount
                ? (0, order_1.mapOrderAmountsFromUnitsToFill)(order, { unitsToFill: takerAmount, totalFilled, totalSize })
                : (0, order_1.mapOrderAmountsFromFilledStatus)(order, { totalFilled, totalSize });
            const { parameters: { offer, consideration } } = orderWithAdjustedFills;
            const orderMetadata = {
                order,
                orderStatus,
                offerCriteria: [],
                considerationCriteria: [],
                tips: [],
                extraData: "0x",
                offererBalancesAndApprovals: "",
                offererOperator: ""
            };
            ordersMetadata.push(orderMetadata);
            offers.push(...offer);
            considerations.push(...consideration);
            const considerationIncludingTips = [
                ...order.parameters.consideration,
                ...tips,
            ];
            const currentBlockTimestamp = new Date().getTime();
            const timeBasedItemParams = {
                startTime: order.parameters.startTime,
                endTime: order.parameters.endTime,
                currentBlockTimestamp,
                ascendingAmountTimestampBuffer: this.config.ascendingAmountFulfillmentBuffer,
                isConsiderationItem: true,
            };
            totalNativeAmount = totalNativeAmount.add((_b = (_a = (0, item_1.getSummedTokenAndIdentifierAmounts)({
                items: considerationIncludingTips,
                criterias: [],
                timeBasedItemParams,
            })[web3_wallets_1.ethers.constants.AddressZero]) === null || _a === void 0 ? void 0 : _a["0"]) !== null && _b !== void 0 ? _b : ethers_1.BigNumber.from(0));
        }
        const advancedOrdersWithTips = ordersMetadata.map(({ order, unitsToFill = 0, tips, extraData }) => {
            const { numerator, denominator } = (0, fulfill_1.getAdvancedOrderNumeratorDenominator)(order, unitsToFill);
            const considerationIncludingTips = [
                ...order.parameters.consideration,
                ...tips,
            ];
            return Object.assign(Object.assign({}, order), { parameters: Object.assign(Object.assign({}, order.parameters), { consideration: considerationIncludingTips, totalOriginalConsiderationItems: order.parameters.consideration.length }), numerator,
                denominator,
                extraData });
        });
        const considerationIncludingTips = [...considerations, ...tips];
        const offerCriteriaItems = offers.filter(({ itemType }) => (0, item_1.isCriteriaItem)(itemType));
        const considerationCriteriaItems = considerationIncludingTips.filter(({ itemType }) => (0, item_1.isCriteriaItem)(itemType));
        const hasCriteriaItems = offerCriteriaItems.length > 0 || considerationCriteriaItems.length > 0;
        const criteriaResolvers = hasCriteriaItems ? (0, criteria_1.generateCriteriaResolvers)({ orders }) : [];
        const { offerFulfillments, considerationFulfillments } = (0, fulfill_1.generateFulfillOrdersFulfillments)(ordersMetadata);
        const fulfillerConduitKey = "0x0000000000000000000000000000000000000000000000000000000000000000";
        recipient = recipient || this.walletInfo.address;
        const maximumFulfilled = advancedOrdersWithTips.length;
        const payableOverrides = { value: totalNativeAmount };
        return this.seaport.populateTransaction.fulfillAvailableAdvancedOrders(advancedOrdersWithTips, criteriaResolvers, offerFulfillments, considerationFulfillments, fulfillerConduitKey, recipient, maximumFulfilled, payableOverrides);
    }
    async fulfillAdvancedOrder({ order, takerAmount, recipient, tips = [], }) {
        var _a;
        const { parameters } = order;
        const orderStatus = await this.getOrderStatus(this.getOrderHash(parameters));
        const { totalFilled, totalSize } = orderStatus;
        const orderWithAdjustedFills = takerAmount
            ? (0, order_1.mapOrderAmountsFromUnitsToFill)(order, { unitsToFill: takerAmount, totalFilled, totalSize })
            : (0, order_1.mapOrderAmountsFromFilledStatus)(order, { totalFilled, totalSize });
        const { parameters: { offer, consideration } } = orderWithAdjustedFills;
        const orderAccountingForTips = Object.assign(Object.assign({}, order), { parameters: {
                parameters,
                consideration: [...order.parameters.consideration, ...tips]
            } });
        const advancedOrder = Object.assign(Object.assign({}, orderAccountingForTips), { numerator: 1, denominator: 1, extraData: "0x" });
        const considerationIncludingTips = [...consideration, ...tips];
        const offerCriteriaItems = offer.filter(({ itemType }) => (0, item_1.isCriteriaItem)(itemType));
        const considerationCriteriaItems = considerationIncludingTips.filter(({ itemType }) => (0, item_1.isCriteriaItem)(itemType));
        const hasCriteriaItems = offerCriteriaItems.length > 0 || considerationCriteriaItems.length > 0;
        const criteriaResolvers = hasCriteriaItems
            ? (0, criteria_1.generateCriteriaResolvers)({ orders: [order] })
            : [];
        const sanitizedOrder = (0, order_1.validateAndSanitizeFromOrderStatus)(order, orderStatus);
        const currentBlockTimestamp = new Date().getTime();
        const timeBasedItemParams = {
            startTime: sanitizedOrder.parameters.startTime,
            endTime: sanitizedOrder.parameters.endTime,
            currentBlockTimestamp,
            ascendingAmountTimestampBuffer: this.config.ascendingAmountFulfillmentBuffer
        };
        const totalNativeAmount = (_a = (0, item_1.getSummedTokenAndIdentifierAmounts)({
            items: considerationIncludingTips,
            criterias: [],
            timeBasedItemParams: Object.assign(Object.assign({}, timeBasedItemParams), { isConsiderationItem: true }),
        })[web3_wallets_1.ethers.constants.AddressZero]) === null || _a === void 0 ? void 0 : _a["0"];
        const payableOverrides = { value: totalNativeAmount };
        const fulfillerConduitKey = web3_wallets_1.NULL_BLOCK_HASH;
        recipient = recipient || this.walletInfo.address;
        return this.seaport.populateTransaction.fulfillAdvancedOrder(advancedOrder, criteriaResolvers, fulfillerConduitKey, recipient, payableOverrides);
    }
    async fulfillBasicOrder({ order, tips = [], }) {
        var _a, _b;
        const conduitKey = await this.conduitController.getKey(this.conduit.address);
        const { parameters } = order;
        const { offer, consideration } = parameters;
        const considerationIncludingTips = [...consideration, ...tips];
        const offerItem = offer[0];
        const [forOfferer, ...forAdditionalRecipients] = considerationIncludingTips;
        const basicOrderRouteType = (_a = constants_1.offerAndConsiderationFulfillmentMapping[offerItem.itemType]) === null || _a === void 0 ? void 0 : _a[forOfferer.itemType];
        if (basicOrderRouteType === undefined) {
            throw new Error("Order parameters did not result in a valid basic fulfillment");
        }
        const orderStatus = await this.seaport.getOrderStatus(this.getOrderHash(parameters));
        const sanitizedOrder = (0, order_1.validateAndSanitizeFromOrderStatus)(order, orderStatus);
        const currentBlockTimestamp = new Date().getTime();
        const timeBasedItemParams = {
            startTime: sanitizedOrder.parameters.startTime,
            endTime: sanitizedOrder.parameters.endTime,
            currentBlockTimestamp,
            ascendingAmountTimestampBuffer: this.config.ascendingAmountFulfillmentBuffer
        };
        const additionalRecipients = forAdditionalRecipients.map(({ startAmount, recipient }) => ({
            amount: startAmount,
            recipient,
        }));
        const considerationWithoutOfferItemType = considerationIncludingTips.filter((item) => item.itemType !== offer[0].itemType);
        const totalNativeAmount = (_b = (0, item_1.getSummedTokenAndIdentifierAmounts)({
            items: considerationWithoutOfferItemType,
            criterias: [],
            timeBasedItemParams: Object.assign(Object.assign({}, timeBasedItemParams), { isConsiderationItem: true }),
        })[web3_wallets_1.ethers.constants.AddressZero]) === null || _b === void 0 ? void 0 : _b["0"];
        const basicOrderParameters = {
            offerer: order.parameters.offerer,
            offererConduitKey: order.parameters.conduitKey,
            zone: order.parameters.zone,
            basicOrderType: order.parameters.orderType + 4 * basicOrderRouteType,
            offerToken: offerItem.token,
            offerIdentifier: offerItem.identifierOrCriteria,
            offerAmount: offerItem.endAmount,
            considerationToken: forOfferer.token,
            considerationIdentifier: forOfferer.identifierOrCriteria,
            considerationAmount: forOfferer.endAmount,
            startTime: order.parameters.startTime,
            endTime: order.parameters.endTime,
            salt: order.parameters.salt,
            totalOriginalAdditionalRecipients: order.parameters.consideration.length - 1,
            signature: order.signature,
            fulfillerConduitKey: conduitKey,
            additionalRecipients,
            zoneHash: order.parameters.zoneHash,
        };
        const payableOverrides = { value: totalNativeAmount };
        return this.seaport.populateTransaction.fulfillBasicOrder(basicOrderParameters, payableOverrides);
    }
    cancelOrders(orders) {
        return this.seaport.cancel(orders);
    }
    bulkCancelOrders() {
        return this.seaport.incrementCounter();
    }
    validate(orders) {
        return this.seaport.validate(orders);
    }
    getOrderStatus(orderHash) {
        return this.seaport.getOrderStatus(orderHash);
    }
    getCounter(offerer) {
        return this.seaport
            .getCounter(offerer)
            .then((counter) => counter.toNumber());
    }
    getOrderHash(orderParameters) {
        return (0, web3_wallets_1.getEIP712StructHash)(constants_1.EIP_712_PRIMARY_TYPE, constants_1.EIP_712_ORDER_TYPE, orderParameters);
    }
    async ethSend(callData) {
        return (0, web3_wallets_1.ethSend)(this.walletInfo, callData).catch(err => {
            throw err;
        });
    }
    async estimateGas(callData) {
        var _a;
        const rpcUrl = ((_a = this.walletInfo.rpcUrl) === null || _a === void 0 ? void 0 : _a.url) || await (0, web3_wallets_1.getChainRpcUrl)(this.walletInfo.chainId);
        return (0, web3_wallets_1.getEstimateGas)(rpcUrl, callData).catch(err => {
            throw err;
        });
    }
}
exports.Seaport = Seaport;
