/// <reference types="node" />
import EventEmitter from 'events';
import { APIConfig, ApproveInfo, BuyOrderParams, CreateOrderParams, OrderSide, SellOrderParams, Token, Web3Accounts } from 'web3-accounts';
import { LimitedCallSpec, WalletInfo, Contract, ethers } from "web3-wallets";
import { BigNumber } from "ethers";
import { ConsiderationItem, OfferItem, Order, OrderComponents, OrderParameters, OrderStatus, OrderWithCounter } from "./types";
export declare function computeFees(recipients: {
    address: string;
    points: number;
}[], tokenTotal: BigNumber, tokenAddress: string): {
    fees: ConsiderationItem[];
    erc20TokenAmount: ethers.BigNumber;
};
export declare const generateRandomSalt: () => string;
export declare class Seaport extends EventEmitter {
    walletInfo: WalletInfo;
    protocolFeePoints: number;
    contractAddresses: any;
    protocolFeeAddress: string;
    zoneAddress: string;
    pausableZoneAddress: string;
    readonly seaport: Contract;
    conduit: Contract;
    conduitController: Contract;
    userAccount: Web3Accounts;
    GasWarpperToken: Token;
    private config;
    private defaultConduitKey;
    constructor(wallet: WalletInfo, config?: APIConfig);
    getOrderApprove({ asset, quantity, paymentToken, startAmount, }: CreateOrderParams, side: OrderSide): Promise<ApproveInfo>;
    createOrder(offer: OfferItem[], consideration: ConsiderationItem[], expirationTime: number, listingTime?: number): Promise<OrderWithCounter>;
    createBuyOrder({ asset, quantity, paymentToken, expirationTime, startAmount }: BuyOrderParams): Promise<OrderWithCounter>;
    createSellOrder({ asset, quantity, paymentToken, expirationTime, startAmount, listingTime }: SellOrderParams): Promise<OrderWithCounter>;
    signOrder(orderParameters: OrderParameters, counter: number): Promise<string>;
    private checkOrderPost;
    getMatchCallData({ order, takerAmount, tips, }: {
        order: Order;
        takerAmount?: string;
        tips?: ConsiderationItem[];
    }): Promise<ethers.PopulatedTransaction>;
    fulfillAvailableAdvancedOrders({ orders, takerAmount, recipient, tips, }: {
        orders: Order[];
        takerAmount?: string;
        recipient?: string;
        tips?: ConsiderationItem[];
    }): Promise<ethers.PopulatedTransaction>;
    fulfillAdvancedOrder({ order, takerAmount, recipient, tips, }: {
        order: Order;
        takerAmount: string;
        recipient?: string;
        tips?: ConsiderationItem[];
    }): Promise<ethers.PopulatedTransaction>;
    fulfillBasicOrder({ order, tips, }: {
        order: Order;
        tips?: ConsiderationItem[];
    }): Promise<ethers.PopulatedTransaction>;
    cancelOrders(orders: OrderComponents[]): any;
    bulkCancelOrders(): any;
    validate(orders: Order[]): any;
    getOrderStatus(orderHash: string): Promise<OrderStatus>;
    getCounter(offerer: string): Promise<number>;
    getOrderHash(orderParameters: OrderParameters): string;
    ethSend(callData: LimitedCallSpec): Promise<ethers.providers.TransactionResponse>;
    estimateGas(callData: LimitedCallSpec): Promise<string>;
}
