"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OPENSEA_API_CONFIG = exports.CHAIN_PATH = exports.OPENSEA_API_TIMEOUT = exports.OPENSEA_API_KEY = void 0;
exports.OPENSEA_API_KEY = "2f6f419a083c46de9d83ce3dbe7db601";
exports.OPENSEA_API_TIMEOUT = 20000;
exports.CHAIN_PATH = {
    1: 'ethereum',
    4: 'rinkeby'
};
exports.OPENSEA_API_CONFIG = {
    1: { apiBaseUrl: 'https://api.opensea.io' },
    4: { apiBaseUrl: 'https://testnets-api.opensea.io' }
};
