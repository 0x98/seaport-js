import { APIConfig } from "../types";
import { AssetCollection, OrdersQueryParams, AssetsQueryParams } from "./types";
import { BaseFetch } from "web3-accounts";
import { OrderV2 } from "./types";
export declare class SeaportAPI extends BaseFetch {
    chainPath: string;
    constructor(config?: APIConfig);
    getAssets(queryParams: AssetsQueryParams, retries?: number): Promise<AssetCollection[]>;
    getOrders(queryParams: OrdersQueryParams, retries?: number): Promise<{
        orders: OrderV2[];
        count: number;
    }>;
    postOrder(orderStr: string, retries?: number): Promise<{
        order: OrderV2;
    }>;
}
