export declare const OPENSEA_API_KEY = "2f6f419a083c46de9d83ce3dbe7db601";
export declare const OPENSEA_API_TIMEOUT = 20000;
export declare const CHAIN_PATH: {
    [key: string]: string;
};
export declare const OPENSEA_API_CONFIG: {
    1: {
        apiBaseUrl: string;
    };
    4: {
        apiBaseUrl: string;
    };
};
