"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SeaportAPI = void 0;
const web3_wallets_1 = require("web3-wallets");
const querystring_1 = __importDefault(require("querystring"));
const config_1 = require("./config");
const web3_accounts_1 = require("web3-accounts");
const constants_1 = require("../constants");
const utils_1 = require("./utils");
const assert_1 = require("../utils/assert");
const { validateOrderV2, validateOrderWithCounter } = assert_1.seaportAssert;
class SeaportAPI extends web3_accounts_1.BaseFetch {
    constructor(config) {
        const chainId = (config === null || config === void 0 ? void 0 : config.chainId) || 1;
        const apiBaseUrl = (config === null || config === void 0 ? void 0 : config.apiBaseUrl) || config_1.OPENSEA_API_CONFIG[chainId].apiBaseUrl;
        super({
            apiBaseUrl,
            apiKey: (config === null || config === void 0 ? void 0 : config.apiKey) || config_1.OPENSEA_API_KEY
        });
        this.chainPath = config_1.CHAIN_PATH[chainId];
        if (config_1.OPENSEA_API_CONFIG[chainId]) {
            this.proxyUrl = config === null || config === void 0 ? void 0 : config.proxyUrl;
            this.apiTimeout = (config === null || config === void 0 ? void 0 : config.apiTimeout) || config_1.OPENSEA_API_TIMEOUT;
        }
        else {
            throw 'OpenseaAPI unsport chainId:' + (config === null || config === void 0 ? void 0 : config.chainId);
        }
    }
    async getAssets(queryParams, retries = 2) {
        const { owner, include_orders, limit, assets } = queryParams;
        const list = assets ? assets.map((val) => {
            return querystring_1.default.stringify(val);
        }) : [];
        const assetList = list.join('&');
        const query = {
            include_orders: include_orders || false,
            limit: limit || 10
        };
        if (owner) {
            query['owner'] = owner;
        }
        const queryUrl = list.length > 0
            ? `${querystring_1.default.stringify(query)}&${assetList}`
            : querystring_1.default.stringify(query);
        try {

            const json = await this.getQueryString('/api/v1/assets', queryUrl);
            return json.assets.map(val => {
                var _a, _b, _c;
                return (Object.assign(Object.assign({}, val.asset_contract), { royaltyFeePoints: Number((_a = val.collection) === null || _a === void 0 ? void 0 : _a.dev_seller_fee_basis_points), protocolFeePoints: Number((_b = val.collection) === null || _b === void 0 ? void 0 : _b.opensea_seller_fee_basis_points), royaltyFeeAddress: (_c = val.collection) === null || _c === void 0 ? void 0 : _c.payout_address, sell_orders: val.sell_orders, seaport_sell_orders: val.seaport_sell_orders, owner: val.owner, token_id: val.token_id }));
            });
        }
        catch (error) {
            this.throwOrContinue(error, retries);
            await (0, web3_wallets_1.sleep)(3000);
            return this.getAssets(queryParams, retries - 1);
        }
    }
    async getOrders(queryParams, retries = 2) {
        const { token_ids, asset_contract_address } = queryParams;
        try {
            const query = {
                token_ids,
                asset_contract_address,
                limit: queryParams.limit || 10
            };
            const orderSide = queryParams.side == web3_accounts_1.OrderSide.Buy ? 'offers' : 'listings';
            const apiPath = `/v2/orders/${this.chainPath}/seaport/${orderSide}`;

            const headers = {
                "X-API-KEY": this.apiKey || config_1.OPENSEA_API_KEY
            };
            const reqList = [await this.get(apiPath, query, { headers })];
            if (queryParams.side == web3_accounts_1.OrderSide.All) {
                const apiPath = `/v2/orders/${this.chainPath}/seaport/offers`;
                reqList.push(await this.get(apiPath, query, { headers }));
            }
            const orders = [];
            for (let i = 0; i < reqList.length; i++) {
                const orderList = reqList[i].orders;
                if (!orderList) {
                    throw new Error('Not  found: no  matching  order  found');
                }
                for (let j = 0; j < orderList.length; j++) {
                    const order = (0, utils_1.deserializeOrder)(orderList[j]);
                    if (validateOrderV2(order)) {
                        orders.push(order);
                    }
                    else {

                    }
                }
            }
            return {
                orders,
                count: orders.length
            };
        }
        catch (error) {
            this.throwOrContinue(error, retries);
            await (0, web3_wallets_1.sleep)(3000);
            return this.getOrders(queryParams, retries - 1);
        }
    }
    async postOrder(orderStr, retries = 2) {
        const order = JSON.parse(orderStr);
        if (!validateOrderWithCounter(order))
            throw validateOrderWithCounter.errors;
        const { parameters, signature } = order;
        try {
            const opts = {
                headers: {
                    'X-API-KEY': this.apiKey || config_1.OPENSEA_API_KEY
                }
            };
            parameters.totalOriginalConsiderationItems = parameters.consideration.length;
            const offer = parameters.offer[0];
            const sidePath = offer.itemType == constants_1.ItemType.ERC20 && offer.identifierOrCriteria == "0" ? 'offers' : 'listings';
            const apiPath = `/v2/orders/${this.chainPath}/seaport/${sidePath}`;
            const result = await this.post(apiPath, order, opts).catch((e) => {

                throw e;
            });
            return result;
        }
        catch (error) {
            this.throwOrContinue(error, retries);
            await (0, web3_wallets_1.sleep)(3000);
            return this.postOrder(orderStr, retries);
        }
    }
}
exports.SeaportAPI = SeaportAPI;
