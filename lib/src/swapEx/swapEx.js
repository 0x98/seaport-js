"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SwapEx = void 0;
const events_1 = __importDefault(require("events"));
const ethers_1 = require("ethers");
const contracts_1 = require("../contracts");
const web3_wallets_1 = require("web3-wallets");
const web3_accounts_1 = require("web3-accounts");
function getValidSwaps(intData, swaps) {
    let bData = intData.toString(2);
    if (bData.length != swaps.length) {
        const diffLen = swaps.length - bData.length;
        if (bData.length > 200)
            throw 'GetValidSwaps error';
        const b0 = Array(diffLen).fill(0).join('');
        bData = `${b0}${bData}`;
    }
    let allValue = ethers_1.ethers.BigNumber.from(0);
    const swapValid = [];
    const swapIsValid = swaps.map((val, index) => {
        const isValid = bData[swaps.length - index - 1] == '1' ? true : false;
        if (isValid) {
            allValue = allValue.add(val.value);
            swapValid.push(val);
        }
        return {
            index,
            isValid,
            swap: val
        };
    });
    return { swapIsValid, swaps: swapValid, value: allValue.toString(), bData };
}
function getSwapsValue(swaps) {
    let value = ethers_1.ethers.BigNumber.from(0);
    swaps.forEach(val => {
        value = value.add(val.value);
    });
    return value;
}
class SwapEx extends events_1.default {
    constructor(wallet) {
        super();
        this.walletInfo = Object.assign(Object.assign({}, wallet), { rpcUrl: web3_wallets_1.CHAIN_CONFIG[wallet.chainId].rpcs[0] });
        this.userAccount = new web3_accounts_1.Web3Accounts(wallet);
        const contractAddr = contracts_1.EXSWAP_CONTRACTS_ADDRESSES[this.walletInfo.chainId];
        if (!contractAddr)
            throw 'ExSwap config error ' + this.walletInfo.chainId;
        this.swapExContract = new ethers_1.ethers.Contract(contractAddr.ExSwap, contracts_1.ContractABI.swapEx.abi, this.userAccount.signer);
        this.contractAddr = contractAddr;
    }
    async batchBuyWithETHSimulate(swaps) {
        if (swaps.length == 0)
            return { swaps: [], value: '0' };
        for (const val of swaps) {
            if (!val.tradeData || !val.value)
                throw 'BatchBuyWithETHSimulate swaps tradeData or value is undefined';
            const funcID = val.tradeData.substring(0, 10);
            if (this.walletInfo.chainId == 1 || this.walletInfo.chainId == 4) {
                if (val.marketId == "0" && funcID != '0xab834bab')
                    throw 'Opensea match function encode error';
                if (val.marketId == "1" && funcID != '0xe7acab24')
                    throw 'Seaport match function encode error';
            }
        }
        const value = getSwapsValue(swaps);
        return new Promise(async (resolve, reject) => {
            const callData = await this.swapExContract.populateTransaction.batchBuyWithETHSimulate(swaps, { value });
            const rpcUrl = await (0, web3_wallets_1.getChainRpcUrl)(this.walletInfo.chainId);
            return (0, web3_wallets_1.getEstimateGas)(rpcUrl, Object.assign(Object.assign({}, callData), { value: value.toString() })).catch(async (err) => {
                if (err.code == '-32000') {

                    const bal = await this.userAccount.getGasBalances({});

                    reject(err.message);
                }
                else {
                    if (err.data.substr(0, 10) == '0x4e487b71') {

                        throw 'BatchBuyWithETHSimulate Panic';
                    }
                    const intData = parseInt(err.data, 16);
                    if (intData == 0)
                        reject('No valid swaps data by batchBuyWithETHSimulate');
                    const swapData = getValidSwaps(intData, swaps);
                    resolve(swapData);
                }
            });
        });
    }
    async buyOneWithETH(swap) {
        var _a;
        const value = ethers_1.ethers.BigNumber.from(swap.value);
        const marketProxy = this.contractAddr[swap.marketId];
        if (!marketProxy)
            `The Market id ${swap.marketId} is invalid in the buy one of eth`;
        const tradeDetail = {
            value: swap.value,
            tradeData: swap.tradeData
        };
        const tx = await this.swapExContract.populateTransaction.buyOneWithETH(marketProxy, tradeDetail, { value });
        const callData = Object.assign(Object.assign({}, tx), { value: (_a = tx.value) === null || _a === void 0 ? void 0 : _a.toString() });
        return (0, web3_wallets_1.ethSend)(this.walletInfo, callData);
    }
    async batchBuyFromSingleMarketWithETH(swaps) {
        var _a;
        const value = getSwapsValue(swaps);
        const marketProxy = this.contractAddr[swaps[0].marketId];
        if (!marketProxy)
            `The Market id ${swaps[0].marketId} is invalid in the  batch buy`;
        const tradeDetails = swaps.map((val) => {
            return {
                value: val.value,
                tradeData: val.tradeData
            };
        });
        const tx = await this.swapExContract.populateTransaction.batchBuyFromSingleMarketWithETH(marketProxy, tradeDetails, { value });
        const callData = Object.assign(Object.assign({}, tx), { value: (_a = tx.value) === null || _a === void 0 ? void 0 : _a.toString() });
        return (0, web3_wallets_1.ethSend)(this.walletInfo, callData);
    }
    async batchBuyWithETH(swaps) {
        var _a;
        const value = getSwapsValue(swaps);
        const tx = await this.swapExContract.populateTransaction.batchBuyWithETH(swaps, { value });
        const callData = Object.assign(Object.assign({}, tx), { value: (_a = tx.value) === null || _a === void 0 ? void 0 : _a.toString() });
        return (0, web3_wallets_1.ethSend)(this.walletInfo, callData);
    }
    async buyNFTsWithETH(swaps) {
        if (!swaps || swaps.length == 0)
            throw 'No valid swap data';
        if (swaps.length == 1) {
            const swap = swaps[0];
            return this.buyOneWithETH(swap);
        }
        if (swaps.length > 1) {
            const marktIds = swaps.map(val => val.marketId);
            const elementsAreEqual = array => array.every(el => el === array[0]);
            if (elementsAreEqual(marktIds)) {
                if (this.walletInfo.chainId == 1 || this.walletInfo.chainId == 4) {
                    return this.batchBuyFromSingleMarketWithETH(swaps);
                }
            }
            else {
                return this.batchBuyWithETH(swaps);
            }
        }
    }
}
exports.SwapEx = SwapEx;
