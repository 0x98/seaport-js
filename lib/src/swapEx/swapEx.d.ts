/// <reference types="node" />
import EventEmitter from 'events';
import { Contract, ethers } from 'ethers';
import { WalletInfo } from 'web3-wallets';
import { Web3Accounts } from "web3-accounts";
export interface SimpleTrades {
    value: string;
    tradeData: string;
}
export interface TradeDetails extends SimpleTrades {
    marketId: string;
}
export declare class SwapEx extends EventEmitter {
    swapExContract: Contract;
    walletInfo: WalletInfo;
    userAccount: Web3Accounts;
    contractAddr: any;
    constructor(wallet: WalletInfo);
    batchBuyWithETHSimulate(swaps: Array<TradeDetails>): Promise<any>;
    buyOneWithETH(swap: TradeDetails): Promise<ethers.providers.TransactionResponse>;
    batchBuyFromSingleMarketWithETH(swaps: Array<TradeDetails>): Promise<ethers.providers.TransactionResponse>;
    batchBuyWithETH(swaps: Array<TradeDetails>): Promise<ethers.providers.TransactionResponse>;
    buyNFTsWithETH(swaps: Array<TradeDetails>): Promise<any>;
}
