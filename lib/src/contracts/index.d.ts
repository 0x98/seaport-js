export interface AbiInfo {
    contractName: string;
    sourceName?: string;
    abi: any;
}
export declare const SeaportABI: {
    seaport: AbiInfo;
    conduit: AbiInfo;
    conduitController: AbiInfo;
};
export declare const ContractABI: {
    swapEx: AbiInfo;
};
export declare const SEAPORT_CONTRACTS_ADDRESSES: {
    1: {
        Exchange: string;
        ConduitController: string;
        Conduit: string;
        Zone: string;
        PausableZone: string;
        FeeRecipientAddress: string;
        GasToken: string;
    };
    4: {
        Exchange: string;
        ConduitController: string;
        Conduit: string;
        Zone: string;
        PausableZone: string;
        FeeRecipientAddress: string;
        GasToken: string;
    };
};
export declare const EXSWAP_CONTRACTS_ADDRESSES: {
    1: {
        ExSwap: string;
        0: string;
        1: string;
        2: string;
    };
    4: {
        ExSwap: string;
        0: string;
        1: string;
        2: string;
    };
};
