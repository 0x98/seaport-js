import { BigNumber, BigNumberish, ContractTransaction, Overrides, PopulatedTransaction } from "ethers";
import { Asset, Token, ExchangeMetadata, APIConfig, MatchOrdersParams } from "web3-accounts";
import { ItemType, OrderType } from "./constants";
export { OrderType };
export { NULL_BLOCK_HASH, NULL_ADDRESS, getProvider, getEstimateGas, ethSend, BigNumber, ETH_TOKEN_ADDRESS, CHAIN_CONFIG, getChainRpcUrl, hexUtils, getEIP712DomainHash, createEIP712TypedData, } from 'web3-wallets';
export type { Signature, WalletInfo, LimitedCallSpec, EIP712TypedData, EIP712Domain } from 'web3-wallets';
export type { Asset, Token, APIConfig, ExchangeMetadata, MatchOrdersParams };
export declare type SeaportConfig = {
    ascendingAmountFulfillmentBuffer?: number;
    balanceAndApprovalChecksOnOrderCreation?: boolean;
    conduitKeyToConduit?: Record<string, string>;
    overrides?: {
        contractAddress?: string;
        defaultConduitKey?: string;
    };
};
export declare type InputCriteria = {
    identifier: string;
    proof: string[];
};
export declare type NftItemType = ItemType.ERC721 | ItemType.ERC1155 | ItemType.ERC721_WITH_CRITERIA | ItemType.ERC1155_WITH_CRITERIA;
export declare enum BasicOrderRouteType {
    ETH_TO_ERC721 = 0,
    ETH_TO_ERC1155 = 1,
    ERC20_TO_ERC721 = 2,
    ERC20_TO_ERC1155 = 3,
    ERC721_TO_ERC20 = 4,
    ERC1155_TO_ERC20 = 5
}
export declare type OfferItem = {
    itemType: ItemType;
    token: string;
    identifierOrCriteria: string;
    startAmount: string;
    endAmount: string;
};
export declare type ConsiderationItem = {
    itemType: ItemType;
    token: string;
    identifierOrCriteria: string;
    startAmount: string;
    endAmount: string;
    recipient: string;
};
export declare type Item = OfferItem | ConsiderationItem;
export declare type OrderParameters = {
    offerer: string;
    offer: OfferItem[];
    consideration: ConsiderationItem[];
    startTime: BigNumberish;
    endTime: BigNumberish;
    orderType: OrderType;
    zone: string;
    zoneHash: string;
    salt: string;
    conduitKey: string;
};
export declare type OrderComponents = OrderParameters & {
    counter: number;
};
export declare type Order = {
    parameters: OrderParameters;
    signature: string;
};
export declare type OrderWithCounter = {
    parameters: OrderComponents;
    signature: string;
};
export declare type OrderStatus = {
    isValidated: boolean;
    isCancelled: boolean;
    totalFilled: BigNumber;
    totalSize: BigNumber;
};
export declare type BasicErc721Item = {
    itemType: ItemType.ERC721;
    token: string;
    identifier: string;
};
export declare type Erc721ItemWithCriteria = {
    itemType: ItemType.ERC721;
    token: string;
    identifiers: string[];
    amount?: string;
    endAmount?: string;
};
declare type Erc721Item = BasicErc721Item | Erc721ItemWithCriteria;
export declare type BasicErc1155Item = {
    itemType: ItemType.ERC1155;
    token: string;
    identifier: string;
    amount: string;
    endAmount?: string;
};
export declare type Erc1155ItemWithCriteria = {
    itemType: ItemType.ERC1155;
    token: string;
    identifiers: string[];
    amount: string;
    endAmount?: string;
};
declare type Erc1155Item = BasicErc1155Item | Erc1155ItemWithCriteria;
export declare type CurrencyItem = {
    token?: string;
    amount: string;
    endAmount?: string;
};
export declare type CreateInputItem = Erc721Item | Erc1155Item | CurrencyItem;
export declare type TipInputItem = CreateInputItem & {
    recipient: string;
};
export declare type ConsiderationInputItem = CreateInputItem & {
    recipient?: string;
};
export declare type CreateOrderInput = {
    conduitKey?: string;
    zone?: string;
    startTime?: string;
    endTime?: string;
    offer: readonly CreateInputItem[];
    consideration: readonly ConsiderationInputItem[];
    counter?: number;
    fees?: readonly Fee[];
    allowPartialFills?: boolean;
    restrictedByZone?: boolean;
    useProxy?: boolean;
    salt?: string;
};
export declare type AdvancedOrder = Order & {
    numerator: BigNumber;
    denominator: BigNumber;
    extraData: string;
};
export declare type FulfillOrdersMetadata = {
    order: Order;
    unitsToFill?: BigNumberish;
    orderStatus: OrderStatus;
    offerCriteria: InputCriteria[];
    considerationCriteria: InputCriteria[];
    tips: ConsiderationItem[];
    extraData: string;
    offererBalancesAndApprovals: any;
    offererOperator: string;
}[];
export declare type TransactionMethods<T = unknown> = {
    buildTransaction: (overrides?: Overrides) => Promise<PopulatedTransaction>;
    callStatic: (overrides?: Overrides) => Promise<T>;
    estimateGas: (overrides?: Overrides) => Promise<BigNumber>;
    transact: (overrides?: Overrides) => Promise<ContractTransaction>;
};
export declare type ExchangeAction<T = unknown> = {
    type: "exchange";
    transactionMethods: TransactionMethods<T>;
};
export declare type Fee = {
    recipient: string;
    basisPoints: number;
};
export declare type InsufficientApprovals = {
    token: string;
    identifierOrCriteria: string;
    approvedAmount: BigNumber;
    requiredApprovedAmount: BigNumber;
    operator: string;
    itemType: ItemType;
}[];
export declare type CreateOrderAction = {
    type: "create";
    getMessageToSign: () => Promise<string>;
    createOrder: () => Promise<OrderWithCounter>;
};
export declare type ApprovalAction = {
    type: "approval";
    token: string;
    identifierOrCriteria: string;
    itemType: ItemType;
    operator: string;
    transactionMethods: any;
};
export declare type CreateOrderActions = readonly [
    ...ApprovalAction[],
    CreateOrderAction
];
export declare type OrderExchangeActions<T> = readonly [
    ...ApprovalAction[],
    ExchangeAction<T>
];
export declare type OrderUseCase<T extends CreateOrderAction | ExchangeAction> = {
    actions: T extends CreateOrderAction ? CreateOrderActions : OrderExchangeActions<T extends ExchangeAction<infer U> ? U : never>;
    executeAllActions: () => Promise<T extends CreateOrderAction ? OrderWithCounter : ContractTransaction>;
};
export interface OpenSeaAccount {
    address: string;
    config: string;
    profileImgUrl: string;
    user: OpenSeaUser | null;
}
export interface OpenSeaUser {
    username?: string;
}
