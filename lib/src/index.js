"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SeaportSDK = void 0;
const events_1 = __importDefault(require("events"));
const seaport_1 = require("./seaport");
const seaport_2 = require("./api/seaport");
const swapEx_1 = require("./swapEx/swapEx");
const web3_accounts_1 = require("web3-accounts");
const assert_1 = require("./utils/assert");
const { validateOrder, validateOrderWithCounter } = assert_1.seaportAssert;
class SeaportSDK extends events_1.default {
    constructor(wallet, config) {
        super();
        const { chainId, address } = wallet;
        let conf = { chainId, account: address };
        if (config) {
            conf = Object.assign(Object.assign({}, conf), config);
        }
        this.contracts = new seaport_1.Seaport(wallet, conf);
        this.api = new seaport_2.SeaportAPI(conf);
        this.swap = new swapEx_1.SwapEx(wallet);
        this.user = new web3_accounts_1.Web3Accounts(wallet);
        this.walletInfo = wallet;
    }
    async getOrderApprove(params, side) {
        return this.contracts.getOrderApprove(params, side);
    }
    async getMatchCallData(params) {
        const { orderStr, takerAmount } = params;
        if (!validateOrderWithCounter(orderStr))
            throw validateOrderWithCounter.errors;
        return this.contracts.getMatchCallData({ order: JSON.parse(orderStr) });
    }
    async createSellOrder(params) {
        return this.contracts.createSellOrder(params);
    }
    async createBuyOrder(params) {
        return this.contracts.createBuyOrder(params);
    }
    async fulfillOrder(orderStr, options) {
        let order = JSON.parse(orderStr);
        if (order.protocolData) {
            order = order.protocolData;
        }
        if (!validateOrder(order))
            throw validateOrder.errors;
        const { takerAmount, taker } = options || {};
        let data;
        if (takerAmount) {
            data = await this.contracts.fulfillAdvancedOrder({ order, takerAmount, recipient: taker });
        }
        else {
            data = await this.contracts.fulfillBasicOrder({ order });
        }
        return this.contracts.ethSend((0, web3_accounts_1.transactionToCallData)(data));
    }
    async fulfillOrders(orders) {
        const { orderList } = orders;
        if (orderList.length == 0) {
            throw 'Seaport fulfill orders eq 0';
        }
        if (orderList.length == 1) {
            const { orderStr, metadata, takerAmount, taker } = orderList[0];
            const oneOption = {
                metadata,
                takerAmount,
                taker
            };
            return this.fulfillOrder(orderStr, oneOption);
        }
        else {
            const availableOrders = [];
            for (const { orderStr } of orderList) {
                let order = JSON.parse(orderStr);
                if (order.protocolData) {
                    order = order.protocolData;
                }
                if (!validateOrder(order))
                    throw validateOrder.errors;
                availableOrders.push(order);
            }
            const data = await this.contracts.fulfillAvailableAdvancedOrders({ orders: availableOrders });
            return this.contracts.ethSend((0, web3_accounts_1.transactionToCallData)(data));
        }
    }
    async cancelOrders(orders) {
        if (orders.length == 0) {
            return this.contracts.bulkCancelOrders();
        }
        else {
            const orderComp = orders.map((val) => {
                const order = JSON.parse(val);
                if (!validateOrderWithCounter(order))
                    throw validateOrderWithCounter.errors;
                const { parameters } = order;
                return parameters;
            });
            return this.contracts.cancelOrders(orderComp);
        }
    }
    async getAssetBalances(asset, account) {
        return this.user.getAssetBalances(asset, account);
    }
    async getTokenBalances(params) {
        return this.user.getTokenBalances({
            tokenAddr: params.tokenAddress,
            account: params.accountAddress,
            rpcUrl: params.rpcUrl
        });
    }
    async transfer(asset, to, quantity) {
        return this.user.transfer(asset, to, quantity);
    }
    async getOwnerAssets(tokens) {
        if (tokens) {
            tokens.owner = tokens.owner || this.walletInfo.address;
        }
        else {
            tokens = {
                owner: this.walletInfo.address,
                limit: 1,
            };
        }
        return this.api.getAssets(tokens);
    }
    async getAssetsFees(tokens) {
        const assets = await this.api.getAssets(tokens);
        return assets.map(val => ({
            royaltyFeeAddress: val.royaltyFeeAddress,
            royaltyFeePoints: val.royaltyFeePoints,
            protocolFeePoints: val.protocolFeePoints,
            protocolFeeAddress: this.contracts.protocolFeeAddress
        }));
    }
}
exports.SeaportSDK = SeaportSDK;
