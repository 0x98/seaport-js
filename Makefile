UNAME = $(shell uname -s)

SED = sed
ifeq ($(UNAME), Darwin)
	SED = gsed
endif

ALL: build

.PHONY: build

build:
	pnpm tsc
	find lib/ -type f -name '*.js' -exec gsed -i -e 's/.*console\..*//' {} +
